# Sistema gerenciador de ocorrências - Grupo 10

> Sistema gerenciador de agentes e ocorrências da prefeitura de Belo Horizonte

## Conteúdo

- [Introdução](#introducao)
- [Funções](#funcoes)
- [Instalação](#instalacao)
- [Autores](#autores)

## Introdução

Este projeto exibe uma lista de agentes da prefeitura de Belo Horizonte sobre um mapa da cidade, bem como eventuais ocorrências que possam acontecer ao redor da cidade.
Foi utilizada a API do Google Maps em javascript, e o template [Light Bootstrap Dashboard](https://www.creative-tim.com/product/light-bootstrap-dashboard).

Para desenvolver este projeto, foram utilizados dados estáticos para representar os agentes e as ocorrências que são exibidos por padrão no mapa. Em ambiente de produção, seria necessário que os dados sejam recebidos no formato especificado dentro do projeto, para que o sistema funcione corretamente.

## Funções

É possível digitar um endereço para encontrar o agente mais próximo a ele, ou adicionar uma ocorrência naquele ponto.

Ao clicar em uma ocorrência, os dados do agente mais próximo são exibidos em um popup no mapa.

É possível alocar/desalocar um agente, e seus dados serão acrescentados ao histórico.

## Instalação

Basta clonar este projeto para sua máquina e começar a usar.

## Autores

[@Feholy](https://github.com/feholy)

[@Izaoli](https://github.com/izaoli)

[@TripCard](https://github.com/tripcard)
