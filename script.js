var agentMarkers = [];
var map;
var iconBase = 'https://maps.google.com/mapfiles/ms/micons/';

var icons = {
    police: {
        icon: iconBase + 'police.png'
    },
    info: {
        icon: iconBase + 'info.png'
    },
    recycle: {
        icon: iconBase + 'recycle.png'
    },
    caution: {
        icon: iconBase + 'caution.png'
    }
};
function initMap() {
    // Create a map object and specify the DOM element for display.
    map = new google.maps.Map(document.getElementById('map'), {
        center: {lat: -19.9180665, lng: -44.1000479},
        zoom: 10
    });


    var agents = [
        {
            position: new google.maps.LatLng(-19.9180665, -44.1000479),
            type: 'recycle',
            nomeFuncionario: 'José Neves',
            contato: '(31) 3471-8798',
            servico: 'SLU',
            matricula: '1'
        }, {
            position: new google.maps.LatLng(-19.8211196, -44.0046162),
            type: 'police',
            nomeFuncionario: 'Júlio Gonçalves',
            contato: '(31) 9878-4141',
            servico: 'GM',
            matricula: '2'
        }, {
            position: new google.maps.LatLng(-19.8821482, -43.9355383),
            type: 'police',
            nomeFuncionario: 'Reinaldo Leite',
            contato: '(31) 9878-4258',
            servico: 'GM',
            matricula: '3'
        }
    ];
    agents.forEach(function (agent) {
        var marker = new google.maps.Marker({
            position: agent.position,
            icon: icons[agent.type].icon,
            map: map
        });
        marker.nomeFuncionario
                = agent.nomeFuncionario;
        marker.contato
                = agent.contato;
        marker.servico
                = agent.servico;
        marker.matricula
                = agent.matricula;
        marker.setTitle(agent.nomeFuncionario);
        var html = '<div id="content">' +
                '<div id="siteNotice">' +
                '</div>' +
                '<h1 id="firstHeading" class="firstHeading">' + agent.nomeFuncionario + '</h1>' +
                '<div id="bodyContent">' +
                '<p><b>Contato:' + agent.contato + '</b></p>' +
                '<p><b>Servico:' + agent.servico + '</b></p>' +
                '<p><b>Status: Disponível</b></p>' +
                '<p><b>Alocar: <a class="btn" onClick="alocar(' + marker.matricula + ')">Sim</a></b></p>' +
                '</div>';
        var infowindow = new google.maps.InfoWindow({
            content: html
        });
        marker.info = infowindow;
        google.maps.event.addListener(marker, 'click', function () {

            infowindow.open(map, marker);
        });
        agentMarkers.push(marker);
    });
}

function findClosestMarker(position) {
    var closestMarker = -1;
    var closestDistance = Number.MAX_VALUE;
    var encontrado
            = false;
    var servicos = [];
    $("input:checkbox[name=setor]:checked").each(function (i, el) {
        servicos.push($(this).val());
    });
    for (var i = 0; i < agentMarkers.length; i++) {
        var distance = google.maps.geometry.spherical.computeDistanceBetween(agentMarkers[i].getPosition(), position);
        if ((distance < closestDistance) & (servicos.includes(agentMarkers[i].servico))) {
            closestMarker = agentMarkers[i];
            closestDistance = distance;
        }
    }
    closestMarker.info.open(map, closestMarker);
}
function getAddress() {
    var end = $('.navbar-default #endereco').val();
    if (end == '') {
        return;
    }
    var url = "http://maps.google.com/maps/api/geocode/json?address=";
    url += end;
    $.get(url, function (data, status) {

        var lat = data.results[0].geometry.location.lat;
        var lng = data.results[0].geometry.location.lng;
        var position = new google.maps.LatLng(lat, lng);
        findClosestMarker(position);
    });
}

function loadOccurrences() {
    var occurrences = [{
            position: new google.maps.LatLng(-19.9592372, -43.9756202),
            type: 'caution'
        }];
    occurrences.forEach(function (occurrence) {
        var marker = new google.maps.Marker({
            position: occurrence.position,
            icon: icons[occurrence.type].icon,
            map: map
        });
        google.maps.event.addListener(marker, 'click', function () {
            findClosestMarker(marker.getPosition());
//            infowindow.setContent(html);
//            infowindow.open(map, marker);
        });
    });
}
function alocar(matricula) {
    agentMarkers.forEach(function (value, key) {
        if (value.matricula == matricula) {
            if (value.alocado != 1) {
                value.alocado = 1;
                var html = '<div id="content">' +
                        '<div id="siteNotice">' +
                        '</div>' +
                        '<h1 id="firstHeading" class="firstHeading">' + value.nomeFuncionario + '</h1>' +
                        '<div id="bodyContent">' +
                        '<p><b>Contato:' + value.contato + '</b></p>' +
                        '<p><b>Servico:' + value.servico + '</b></p>' +
                        '<p><b>Status: Alocado</b></p>' +
                        '<p><b>Desalocar: <a class="btn" onClick="alocar(' + value.matricula + ')">Sim</a></b></p>' +
                        '</div>';
                value.info.setContent(html);

                popularHistorico('Funcionário: ' + value.nomeFuncionario + ' alocado!');
                return false;
            } else {
                value.alocado = 0;
                var html = '<div id="content">' +
                        '<div id="siteNotice">' +
                        '</div>' +
                        '<h1 id="firstHeading" class="firstHeading">' + value.nomeFuncionario + '</h1>' +
                        '<div id="bodyContent">' +
                        '<p><b>Contato:' + value.contato + '</b></p>' +
                        '<p><b>Servico:' + value.servico + '</b></p>' +
                        '<p><b>Status: Disponível</b></p>' +
                        '<p><b>Alocar: <a class="btn" onClick="alocar(' + value.matricula + ')">Sim</a></b></p>' +
                        '</div>';
                value.info.setContent(html);
                popularHistorico('Funcionário: ' + value.nomeFuncionario + ' disponível!');
                return false;
            }
        }
    });
}
function filtrar() {
    var servicos = [];
    $("input:checkbox[name='setor']:checked").each(function (i, el) {
        if (!servicos.includes($(this).val())) {
            servicos.push($(this).val());
        }
    });
    for (var i = 0; i < agentMarkers.length; i++) {
        if (!servicos.includes(agentMarkers[i].servico)) {
            agentMarkers[i].setMap(null);
        } else {
            agentMarkers[i].setMap(map);
        }
    }
}

function popularHistorico(msg) {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!
    if (dd < 10) {
        dd = '0' + dd
    }
    if (mm < 10) {
        mm = '0' + mm
    }
    var dateTime = mm + '/' + dd + '/' + today.getFullYear() + ' ' + today.getHours() + ':' + today.getMinutes() + ':' + today.getSeconds();
    var html = '<li>'
            + '<a href="javascript:void(0);">'
            //+ '<i class="pe-7s-graph"></i>'
            + '<p>' + dateTime + ' <br/> ' + msg + '</p>'
            + '</a>'
    '</li>';
    $('#historicoContainer').prepend(html);
    //$('#alocados').html($('#alocados').html() + '<br />' + msg + '<hr />');
}

$(document).on('click', '.serviceType', function () {
    var elem = $(this).find('input[type="checkbox"]');
    $('input[value="' + elem.val() + '"]').prop('checked', !elem.is(':checked')).trigger('change');
    if (elem.is(':checked')) {
        $(this).addClass('active');
    } else {
        $(this).removeClass('active');
    }
});

$(document).on('change', '.serviceType input', function () {
    filtrar();
});